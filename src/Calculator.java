
public class Calculator {
	public static void main(String[] args) throws Exception {
//		Calculator c = new Calculator();
//		int test= c.Add("//;\\n-2;4;4;-4");
//		System.out.print(test);

	}

	private String[] input;
	private String delimiter;
	private String s;

	public int Add(String numbers) throws Exception {
		int result = 0;
		if(!numbers.isEmpty()) {
			if(numbers.startsWith("//")) {
				delimiter = numbers.substring(2,3);
				s = numbers.substring(5);
				input = s.split(delimiter);
			}
			else {
				input = numbers.split(",|\\\n");
			}
			for(int i=0; i< input.length; i++) {
				if(Integer.parseInt(input[i]) < 0){
					String negativeNumbers="";
					for (int j=i; j< input.length; j++) {
						if (Integer.parseInt(input[j])<0) {
						negativeNumbers=negativeNumbers + input[j] + " ";
						}
					}
					throw new Exception("Negative not allowed " + negativeNumbers);
				}
				result = result+Integer.parseInt(input[i]);

			}
		}

		return result;
	}


}
