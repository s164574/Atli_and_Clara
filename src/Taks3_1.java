

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;

public class Taks3_1 {
	@Before
	public void setUp() {
		//		Calculator calculator = new Calculator();
	}

	@Test
	public void TestSimple() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(0,calculator.Add(""));
	}

	@Test
	public void TestOneNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(2,calculator.Add("2"));
	}

	@Test
	public void TestTwoNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(5,calculator.Add("2,3"));
	}
	@Test
	public void TestTwoNumber2() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(9,calculator.Add("4,5"));
	}

	@Test
	public void TestThreeNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(6,calculator.Add("1,2,3"));
	}

	@Test
	public void TestFourNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(11,calculator.Add("1,2,3,5"));
	}

	@Test
	public void TestNewLineNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(11,calculator.Add("1\n2\n3\n5"));
	}

	@Test
	public void TestCommaNewLineNumber() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(11,calculator.Add("1\n2,3\n5"));
	}

	@Test
	public void TestNewDelimiterTwoNumbers() throws Exception {
		Calculator calculator = new Calculator();
		assertEquals(14,calculator.Add("//;\\n2;4;4;4"));
	}

	@Rule
    public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void TestNegativesNotAllowed() throws Exception {
		Calculator calculator = new Calculator();
		exception.expectMessage("Negative not allowed -2 -4");
		calculator.Add("//;\\n-2;4;4;-4");
    }
}


